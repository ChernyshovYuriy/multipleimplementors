cmake_minimum_required(VERSION 3.14)
project(multipleimplementors)

set(CMAKE_CXX_STANDARD 11)

file(GLOB_RECURSE SOURCE_FILES "src/cpp/*")

add_executable(multipleimplementors ${SOURCE_FILES})
