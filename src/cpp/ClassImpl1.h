#ifndef MULTIPLEIMPLEMENTORS_CLASSIMPL1_H
#define MULTIPLEIMPLEMENTORS_CLASSIMPL1_H

#include <memory>
#include "AbstractClass.h"

using namespace std;

struct ClassImpl1Struct;

class ClassImpl1 : public AbstractClass {

public:
    ClassImpl1();
    ~ClassImpl1();
    int methodA() override;

private:
    unique_ptr<ClassImpl1Struct> internalData;
};


#endif //MULTIPLEIMPLEMENTORS_CLASSIMPL1_H
