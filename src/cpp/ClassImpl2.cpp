#include "ClassImpl2.h"

struct ClassImpl2Struct {
    int value = 0;
};

ClassImpl2::ClassImpl2() : internalData(unique_ptr<ClassImpl2Struct>(new ClassImpl2Struct)) {
    internalData->value = 2;
}

ClassImpl2::~ClassImpl2() = default;

int ClassImpl2::methodA() {
    return internalData->value;
}
