#ifndef MULTIPLEIMPLEMENTORS_CLASSIMPL2_H
#define MULTIPLEIMPLEMENTORS_CLASSIMPL2_H

#include <memory>
#include "AbstractClass.h"

using namespace std;

struct ClassImpl2Struct;

class ClassImpl2 : public AbstractClass {

public:
    ClassImpl2();
    ~ClassImpl2();
    int methodA() override;

private:
    unique_ptr<ClassImpl2Struct> internalData;
};


#endif //MULTIPLEIMPLEMENTORS_CLASSIMPL2_H
