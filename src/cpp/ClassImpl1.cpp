#include "ClassImpl1.h"

struct ClassImpl1Struct {
    int value = 0;
};

ClassImpl1::ClassImpl1() : internalData(unique_ptr<ClassImpl1Struct>(new ClassImpl1Struct)) {
    internalData->value = 1;
}

ClassImpl1::~ClassImpl1() = default;

int ClassImpl1::methodA() {
    return internalData->value;
}


