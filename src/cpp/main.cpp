#include <iostream>
#include <vector>
#include <memory>
#include "AbstractClass.h"
#include "ClassImpl1.h"
#include "ClassImpl2.h"

using namespace std;

int main() {
    vector<shared_ptr<AbstractClass>> data;
    data.push_back(shared_ptr<AbstractClass>(new ClassImpl1()));
    data.push_back(shared_ptr<AbstractClass>(new ClassImpl2()));
    return 0;
}
